import React from 'react';
import one from './assets/one.jpg';

const Placeholder1 = () => {
  return (
    <div style={{padding: '20px 40px'}}>
      <img src={one} style={{width: '100%'}} alt='' />
    </div>
  );
}

export default Placeholder1;