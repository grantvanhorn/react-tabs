import React from 'react';
import three from './assets/three.png';

const Placeholder2 = () => {
  return (
    <div style={{padding: '20px 40px'}}>
      <img src={three} style={{width: '100%'}} alt='' />
    </div>
  )
}

export default Placeholder2;