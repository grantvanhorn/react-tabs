import React, { Component } from 'react';
import Tabs from './Tabs';

import Placeholder1 from './Placeholder1';
import Placeholder2 from './Placeholder2';
import Placeholder3 from './Placeholder3';
import Placeholder4 from './Placeholder4';

import {
  faCoffee,
  faAddressBook,
  faBasketballBall,
  faBookmark
} from '@fortawesome/free-solid-svg-icons';

import './App.css';

const tabsConfig = [
  {
    title: 'Test Tab One',
    component: Placeholder1,
    icon: faCoffee,
  },
  {
    title: 'Tab Two',
    component: Placeholder2,
    icon: faAddressBook,
  },
  {
    title: 'Three',
    component: Placeholder3,
    icon: faBasketballBall,
  },
  {
    title: 'Another One',
    component: Placeholder4,
    icon: faBookmark,
  },
];

class App extends Component {
  render() {
    return (
      <div className="App">
        <Tabs config={tabsConfig} />
      </div>
    );
  }
}

export default App;
