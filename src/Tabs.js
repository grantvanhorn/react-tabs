import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TabHeader from './TabHeader';
import './Tabs.scss';

class Tabs extends Component {
  constructor() {
    super();
    this.state = {
      activeIndex: 0,
    }
  }

  selectTab = (activeIndex) => {
    this.setState({ activeIndex });
  }

  renderTabs() {
    const { config } = this.props;
    const { activeIndex } = this.state;
    return config.map((tab, index) => (
      <TabHeader
        key={index}
        active={index === activeIndex}
        index={index}
        select={this.selectTab}
        tabConfig={tab}
      />
    ));
  }

  renderContent() {
    const { activeIndex } = this.state;
    const { config } = this.props;
    const { component: Component } = config[activeIndex];
    return <Component />;
  }

  render() {
    const { config} = this.props;
    if (config.length < 1) return null;

    return (
      <div className='tabs-wrapper'>
        <div className='tab-headers'>
          {this.renderTabs()}
        </div>
        <div className='tab-content'>
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

Tabs.propTypes = {
  config: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    component: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
  })).isRequired,
};

export default Tabs;
