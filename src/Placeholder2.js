import React from 'react';
import two from './assets/two.png';

const Placeholder2 = () => {
  return (
    <div style={{padding: '20px 40px'}}>
      <img src={two} style={{width: '100%'}} alt='' />
    </div>
  )
}

export default Placeholder2;