import React from 'react';
import four from './assets/four.png';

const Placeholder2 = () => {
  return (
    <div style={{padding: '20px 40px'}}>
      <img src={four} style={{width: '100%'}} alt='' />
    </div>
  )
}

export default Placeholder2;