import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './TabHeader.scss';

const TabHeader = ({ active, index, select, tabConfig,  }) => {
  const { title, icon } = tabConfig;

  let className = 'header-wrapper';
  if (active) className += ' active';

  return (
    <div
      className={className}
      onClick={() => select(index)}
    >
      <div className='active-bar' />
      <div className='icon'>
        <FontAwesomeIcon icon={icon} />
      </div>
      <div className='title'>
        { title }
      </div>
    </div>
  );
}

TabHeader.propTypes = {
  active: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
  select: PropTypes.func.isRequired,
  tabConfig: PropTypes.shape({
    title: PropTypes.string.isRequired,
    component: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
  }).isRequired,
};

export default TabHeader;